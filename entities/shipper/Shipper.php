<?php 
class Shipper{
    public $base_url;
    public $homedir;


	private static $instance = null;
    private function __construct($cfg){
        $this->base_url = $cfg->getBaseurl();
        $this->homedir = $cfg->getHomedir();
    }

    public static function getInstance($cfg){
    	if(self::$instance==null){
    		self::$instance=new Shipper($cfg);
    	}
    	return self::$instance;
    }

    public function Tes(){
    	return "hello!";
    }

    public function Model($file){
        include_once $this->homedir.'model/'.$file.'.php';
    }

    public function View($path,$data){
        include_once $this->homedir.'view/'.$path.'.php';
    }

    public function Lib($file){
        include_once $this->homedir.'libraries/'.$file.'.php';
    }

    public function JustInclude($path){
        include_once $this->homedir.$path.'.php';   
    }

}