<?php 
include_once Mastercfg::$homedir.'entities/shipper/Shipper.php';
class Homepagectr {
	private static $instance = null;
    private function __construct(){
        
    }

    public static function getInstanceOfHomepagectr(){
    	if (self::$instance==null) {
			self::$instance = new Homepagectr();
		}
		return self::$instance;
    }

    private function newSummaryPost($masukdata){
        $data = Shipper::getInstance(Mastercfg::getCfgInstance());
        $data->Model('Querybuilder');
        $db = Querybuilder::getQueryBuilderInstance();

        $r = $db->InsertData('zeke.summary_spec',['sdetail'=>$masukdata['summarylabel']]);

        if ($r) {
            echo "<script>alert('Data submitted.');</script>";
            echo "<script>window.close();</script>";
        } else {
            echo "<script>alert('ERROR: Data failed to be submitted.');</script>";
        }

    }

    public function newSummarySpecific(){
        $data = Shipper::getInstance(Mastercfg::getCfgInstance());
        $data->Model('Querybuilder');
        $db = Querybuilder::getQueryBuilderInstance();

        if (isset($_POST['in'])) {
            $this->newSummaryPost($_POST['in']);
        }

        $data->View('homepage/vnewdata.sumspec',$data);
    }

    public function ShowSummarySpecific($id){
        $data = Shipper::getInstance(Mastercfg::getCfgInstance());
        $data->Model('Querybuilder');
        $data->Lib('functions/Fjsonio');
        $db = Querybuilder::getQueryBuilderInstance();
        $db->setOrder('lseqid','ASC');
        $data->listdata = $db->FetchWhere(['lseqid','sdetail'],'zeke.summary_spec',array($db->GetCond('istatus','=',$id)));


        header('Content-Type: application/json');
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        echo GetJsonOutput($data->listdata);
    }

    public function DisplayRemedy(){
        $data = Shipper::getInstance(Mastercfg::getCfgInstance());
        $data->Model('Remedymdl');
        $remedy = Remedymdl::getRemedymdlInstance();
        $data->listdata = $remedy->ShowdatasCurrentMonth();

        $data->View('homepage/table_data.homepage',$data);
    }

    public function EditRemedy($incidentid){
        $data = Shipper::getInstance(Mastercfg::getCfgInstance());
        $data->Model('Querybuilder');
    }

    public function IndexHomepage(){
    	$data = Shipper::getInstance(Mastercfg::getCfgInstance());
        $data->Model('Querybuilder');
        $db = Querybuilder::getQueryBuilderInstance();

        $data->listactions = $db->Fetch('zeke.statusreason');

        $db->setOrder('lseqid','ASC');

        $data->listsummary = $db->Fetch('zeke.summary_spec');

        if (isset($_POST['in'])) {
            // print_r($_POST['in']);
            $r = $db->InsertData('zeke.generalreport',
                [
                    'sincid'=>trim($_POST['in']['inccode']),
                    'scustname'=>trim($_POST['in']['custname']),
                    'dincdate'=>$_POST['in']['incdate'],
                    'sproblem'=>$_POST['in']['incproblem'],
                    'smsisdn'=>$_POST['in']['msisdn'],
                    'srootcause'=>$_POST['in']['rcause'],
                    'ssolution'=>$_POST['in']['incsolution'],
                    'iaction'=>$_POST['in']['incact'],
                    'lfksumspec'=>$_POST['in']['incsumspec']
                ]
            );

            if ($r) {
                echo "<script>alert('New Incident has been submitted!')</script>";
            } else {
                echo "<script>alert('Ooops, error occured!')</script>";
            }
        }

        $data->View('homepage/index.homepage',$data);
    }
}