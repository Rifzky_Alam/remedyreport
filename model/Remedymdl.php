<?php 
include_once 'Koneksi.php';
class Remedymdl extends Koneksi{
    private static $instance=null;
    private $dbHost;

    public static function getRemedymdlInstance(){
    	if(self::$instance==null){
    		self::$instance=new Remedymdl();
    	}
    	return self::$instance;
    }


    public function ShowdatasCurrentMonth(){
    	$query="select sincid,scustname,srootcause,statusreason.sdesc as sdesc, date_format(generalreport.dtimestamp,'%a, %e %b %Y') as dtimestamp
		from generalreport 
		left join statusreason on statusreason.lseqid=generalreport.iaction
		where month(generalreport.dtimestamp)=month(curdate())
		and year(generalreport.dtimestamp)=year(curdate());";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    private function __construct(){
        $this->dbHost = $this->bukaKoneksi();
    }
}

?>