<!DOCTYPE html>
<html>
<head>
	<title>Table Incident</title>
	<link rel="stylesheet" type="text/css" href="<?= $data->base_url ?>assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
	<div class="container" style="padding-top:40px;padding-bottom:40px">
		<div class="row">
			<div class="page-header">
				<h2>Table Incident</h2>		
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<a href="#modal-cariz" data-toggle="modal" class="btn btn-lg btn-primary" >Search</a>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Incident</th>
							<th>Cust Name</th>
							<th>Root Cause</th>
							<th>Action taken</th>
							<th>Timestamp</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if (count($data->listdata)==''): ?>
							<tr>
								<td colspan="6" style="text-align:center;">No data to display.</td>
							</tr>
						<?php else: ?>
							<?php foreach ($data->listdata as $key): ?>
								<tr>
									<td><?= $key['sincid'] ?></td>
									<td><?= $key['scustname'] ?></td>
									<td><?= $key['srootcause'] ?></td>
									<td><?= $key['sdesc'] ?></td>
									<td><?= $key['dtimestamp'] ?></td>
									<td><a href="#">Edit</a></td>
								</tr>
							<?php endforeach ?>
						<?php endif ?>
					</tbody>
				</table>
			</div>
		</div>

		

		
	</div> <!-- end container -->
	<!-- Modal -->
  <div class='modal fade' id='modal-cariz' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Search Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            
					<div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">CustName</label>
                            <input type="text" name="custname" id="custname" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Problem</label>
                            <input type="text" name="sproblem" id="problem" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Error Message</label>
                            <input type="text" name="srootcause" id="errmessage" class="form-control" />
                        </div>
                    </div>              

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">From Date</label>
                            <input type="text" name="tanggalAwal" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">To Date</label>
                            <input type="text" name="tanggalAkhir" id="tanggal-akhir" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-primary" id='btn-downloadLaporan' style="width:100%">Go</button>
                    </div>

                
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>Remedy Report 2020</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
	<script src="<?= $data->base_url ?>assets/jquery/jquery-3.4.1.min.js" type="text/javascript"></script>
	<script src="<?= $data->base_url ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>