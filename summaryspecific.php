<?php 
include_once 'config/Mastercfg.php';
include_once Mastercfg::$homedir.'controller/homepage.controller.php';
$homepage = Homepagectr::getInstanceOfHomepagectr();


if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'newdata':
			$homepage->newSummarySpecific();
			break;
		default:
			header('Location: '.Mastercfg::$base_url.'/error/404/');
			break;
	}
}else{
	header('Location: '.Mastercfg::$base_url.'/error/404/');
}