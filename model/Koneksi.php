<?php 
// include_once '../config/Mastercfg.php';
class Koneksi{

	private static $instance = null;

	private function __construct(){
		
	}

	public static function getInstance(){
		if (self::$instance==null) {
			$conn = new Koneksi();
			self::$instance = $conn->bukaKoneksi();
		}
		return self::$instance;
	}

	public function bukaKoneksi(){
		try {

			@$dbHost= new PDO('pgsql:host='.Mastercfg::$databasehost.';port='.Mastercfg::$databaseport.';dbname='.Mastercfg::$database,Mastercfg::$databaseusername,Mastercfg::$databasepassword);
			$dbHost->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
			return $dbHost;	
		} catch (PDOException $e) {
			echo $e;
			die ('Database Connection Error..');
			return null;
		}
	}

}