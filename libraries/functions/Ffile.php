<?php
function UploadPic($ifile,$folder){
	if (file_exists($folder.$ifile['name'])) {
		unlink($folder.$ifile['name']);
	}

	if (move_uploaded_file($ifile['tmp_name'], $folder.$ifile['name'])) {
		return true;
	}else {
		return false;
	}
}

function CheckExtension($ifile,$allowedextension){
	$filex = pathinfo($ifile['name'],PATHINFO_EXTENSION);
	return in_array($filex, $allowedextension);
}


function ScanDirectory($dir){
	$files = scandir($dir);
    $files = array_diff($files, array('.', '..'));
    return $files;
}

function DeleteFile($folder,$file){
	return unlink($folder.'/'.$file);
}