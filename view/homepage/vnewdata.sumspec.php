<!DOCTYPE html>
<html>
<head>
	<title>Input New Summary Specific</title>
	<link rel="stylesheet" type="text/css" href="<?= $data->base_url ?>assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
	<div class="container" style="padding-top:40px;padding-bottom:40px">

		<h2>New Summay Specific</h2>

		<form action="" method="post" accept-charset="utf-8">
			<div class="form-group">
				<label>Summary Label</label>
				<textarea name="in[summarylabel]" class="form-control" placeholder="Summary Label" required></textarea>
			</div>
			
			<button class="btn btn-lg btn-warning">Submit</button>
		</form>
		<br>

		
	</div> <!-- end container -->
	
	<script src="<?= $data->base_url ?>assets/jquery/jquery-3.4.1.min.js" type="text/javascript"></script>
	<script src="<?= $data->base_url ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	
</body>
</html>