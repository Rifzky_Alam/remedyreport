<!DOCTYPE html>
<html>
<head>
	<title>Input New Incident</title>
	<link rel="stylesheet" type="text/css" href="<?= $data->base_url ?>assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
	<div class="container" style="padding-top:40px;padding-bottom:40px">

		<h2>New Incident</h2>

		<form action="" method="post" accept-charset="utf-8">
			<div class="form-group">
				<label>Incident Number</label>
				<input type="text" name="in[inccode]" class="form-control" placeholder="Incident code" required>
			</div>

			<div class="form-group">
				<label>Customer Name</label>
				<input type="text" name="in[custname]" class="form-control" placeholder="Customer Name" required>
			</div>
			
			<div class="form-group">
				<label>Incident Date</label>
				<input type="text" name="in[incdate]" class="form-control" placeholder="Incident Date" required>
			</div>

			<div class="form-group">
				<label>Problem</label>
				<textarea class="form-control" rows="7" name="in[incproblem]"></textarea>
			</div>

			<div class="form-group">
				<label>Msisdn</label>
				<input type="text" name="in[msisdn]" class="form-control" placeholder="Msisdn" required>
			</div>

			<div class="form-group">
				<label>Error Message</label>
				<input type="text" name="in[rcause]" class="form-control" placeholder="Error Message, type - as default value" required>
			</div>
			

			<div class="form-group">
				<label>
					<a href="<?= $data->base_url ?>summaryspecific/newdata" target="_blank">
					Category
					</a> <span id="refreshcat" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-refresh"></i></span>
				</label>
				<select id="catt" class="form-control" name="in[incsumspec]">
					<option class="rmcat" data-content="" value="0">--No Category--</option>
					<?php foreach ($data->listsummary as $key): ?>
						<option class="rmcat" data-content="<?= $key['sdetail'] ?>" value="<?= $key['lseqid'] ?>">
							<?= $key['sdetail'] ?>
						</option>
					<?php endforeach ?>
				</select>
			</div>

			<div class="form-group">
				<label>Solution</label>
				<textarea id="sol" class="form-control" name="in[incsolution]"></textarea>
			</div>

			<div class="form-group">
				<label>Action</label>
				<select class="form-control" name="in[incact]">
					<?php foreach ($data->listactions as $key): ?>
						<option value="<?= $key['lseqid'] ?>"><?= $key['sdesc'] ?></option>
					<?php endforeach ?>
				</select>
			</div>
			
			<button class="btn btn-lg btn-warning">Submit</button>
		</form>
		<br>

		
	</div> <!-- end container -->
	
	<script src="<?= $data->base_url ?>assets/jquery/jquery-3.4.1.min.js" type="text/javascript"></script>
	<script src="<?= $data->base_url ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$('#catt').change(function(){
			// alert($(this).find(':selected').attr('data-content'));
			$('#sol').html($(this).find(':selected').attr('data-content'));
		});

		$('#refreshcat').click(function(){
			$('.rmcat').remove();
			$.ajax({
            	type: "GET",
            	url: "<?= $data->base_url ?>myio/summaryspecific",
            	data: {
              		'id':'1'
            	},
            	cache: false,
            	success: function(data){
	              // console.log(data);
	              
	              	var myjson = data;
	              	var result = myjson.results;
	              	document.getElementById("catt").innerHTML = "<option class='rmcat' selected data-content='' value='0'>--No Category--</option>";
	              	for (x in result) {
	                	document.getElementById("catt").innerHTML += "<option class='rmcat' data-content='" + result[x].sdetail + "' value='" + result[x].lseqid + "'>" + result[x].sdetail + '</option>';
            		}
            	}

          	}); //end ajax
		});
	</script>
</body>
</html>