<?php 

class Mastercfg{
	public static $instance = null;
	public static $base_url = 'http://10.80.32.66:81/remedyreport/';
	// public static $homedir = 'C:/xampp/htdocs/customframework/'; // windows
	public static $homedir = 'C:/xampp/htdocs/remedyreport/'; //mac
	public static $database = 'postgres';
	public static $databasehost = 'localhost';
	public static $databaseport = '5432';
	public static $databaseusername = 'postgres';
	public static $databasepassword = 'england';
	
	public function getBaseurl(){
		return self::$base_url;
	}

	public function getHomedir(){
		return self::$homedir;
	}

	private function __construct(){}

	public static function getCfgInstance(){
    	if(self::$instance==null){
    		self::$instance=new Mastercfg();
    	}
    	return self::$instance;
    }

}